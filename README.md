# Visualizing auto encoder embeddings

This project implements and applies 4 different convolutional auto encoders: 

*Regular
*Denoising
*Regularized
*Variational autoencoder

To both natural and medical image data sets. The main goal is to compare embeddings to that of a fully supervised CNN's final layer, trained on the available labels. 

## Getting Started

To be added

### Prerequisites

Find all prerequisites in the requirements.txt.

Main packages used are:

```
Python==3.6.7
Tensorflow==2.0 
```

### Installing

To be added

## Running the tests

Several auto-encoders were evaluated. The one minimizing the validation error was selected.

![alt text](http://url/to/img.png)  

## Authors

* **Olle Holmberg**: https://www.linkedin.com/in/olle-holmberg-2ba23152/

## Acknowledgments

To be added