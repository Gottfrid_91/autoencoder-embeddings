from __future__ import absolute_import
from __future__ import division
from __future__ import print_function
from tensorflow.keras.losses import mse
from tensorflow.keras import backend as K
from input import DataGen
from utils import Params, TrainOps, Logging, Evaluation
from autoencoder import encoder, decoder

params = Params("./params.json")
trainops = TrainOps(params)
logging = Logging("./logs", params)
datagen = DataGen(params)
evaluation = Evaluation(params)

image_size = datagen.x_train.shape[1]


filters = [16, 32, 64, 128]
sparse = [False]
regs = [10e-5, 10e-4, 10e-3]
latent_dimensions = [2, 64, 128]


for s in sparse:
    for filter_ in filters:
        for ld in latent_dimensions:
            logging.create_model_directory()

            # set params
            # network parameters
            params.input_shape = (image_size, image_size, 3)
            params.latent_dim = ld
            params.sparse = s
            params.num_filters = filter_
            params.model_dir = logging.model_directory

            # saving model config file to model output dir
            logging.save_dict_to_json(logging.model_directory + "/config.json")

            model_name = 'model.h5'

            logging.create_model_directory()
            params.model_dir = logging.model_directory

            # load model
            encoder_model, inputs, latent, shape = encoder(params)
            vae, decoder_model, outputs = decoder(params, inputs, shape, encoder_model)

            models = (encoder_model, decoder_model)
            data = (datagen.x_test, datagen.y_test)

            # define loss terms
            reconstruction_loss = mse(K.flatten(inputs), K.flatten(outputs))

            reconstruction_loss *= image_size * image_size
            kl_loss = 1 + latent[1] - K.square(latent[0]) - K.exp(latent[1])
            kl_loss = K.sum(kl_loss, axis=-1)
            kl_loss *= -0.5
            vae_loss = K.mean(reconstruction_loss + kl_loss)
            vae.add_loss(vae_loss)
            vae.compile(optimizer='adam')
            vae.summary()

            # define callbacks
            cb = trainops.callbackslist(logging.model_directory)

            # train the auto-encoder

            vae.fit(datagen.x_train,
                    epochs=params.epochs,
                    batch_size=params.batch_size,
                    validation_data=(datagen.x_test, None),
                    callbacks = cb)

            evaluation.plot_results(models, datagen, batch_size=params.batch_size)