from tensorflow.keras.layers import Input, Dense, Conv2D, Conv2DTranspose, Flatten, Reshape, Lambda
from tensorflow.keras.models import Model
from utils import Params, TrainOps, Logging, Evaluation
from tensorflow.keras import backend as K
from tensorflow.keras import regularizers

params = Params("./params.json")
trainops = TrainOps(params)
logging = Logging("./logs", params)
evaluation = Evaluation(params)


def encoder(params):
    # VAE model = encoder + decoder
    # build encoder model
    inputs = Input(shape = params.input_shape, name = 'encoder_input')

    x = inputs
    for i in range(2):
        params.num_filters *= 2
        x = Conv2D(filters = params.num_filters,
                   kernel_size = params.kernel_size,
                   activation = 'relu',
                   strides = 2,
                   padding = 'same')(x)

    # add additional conv layers, reducing dim
    for i in range(2):
        x = Conv2D(filters = params.num_filters,
                   kernel_size = params.kernel_size,
                   activation = 'relu',
                   strides = 2,
                   padding = 'same')(x)

    # shape info needed to build decoder model
    shape = K.int_shape(x)

    # generate latent vector Q(z|X)
    x = Flatten()(x)

    if params.sparse:
        x = Dense(16, activation = 'relu', activity_regularizer = regularizers.l1(params.reg))(x)

    if not params.sparse:
        x = Dense(16, activation = 'relu')(x)

    z_mean = Dense(params.latent_dim, name = 'z_mean')(x)
    z_log_var = Dense(params.latent_dim, name = 'z_log_var')(x)

    # use reparameterization trick to push the sampling out as input
    # note that "output_shape" isn't necessary with the TensorFlow backend
    z = Lambda(evaluation.sampling, output_shape = (params.latent_dim,), name = 'z')([z_mean, z_log_var])

    # instantiate encoder model
    encoder_model = Model(inputs, [z_mean, z_log_var, z], name = 'encoder')
    encoder_model.summary()
    return encoder_model, inputs, [z_mean, z_log_var, z], shape


def decoder(params, inputs, shape, encoder_model):
    # build decoder model
    latent_inputs = Input(shape = (params.latent_dim,), name = 'z_sampling')
    x = Dense(shape[1] * shape[2] * shape[3], activation = 'relu')(latent_inputs)
    x = Reshape((shape[1], shape[2], shape[3]))(x)

    for i in range(4):
        x = Conv2DTranspose(filters = params.num_filters,
                            kernel_size = params.kernel_size,
                            activation = 'relu',
                            strides = 2,
                            padding = 'same')(x)

        # add extra conv layers for processing
        x = Conv2D(filters = params.num_filters,
                   kernel_size = params.kernel_size,
                   activation = 'relu',
                   strides = 1,
                   padding = 'same')(x)
        params.num_filters //= 2

    outputs = Conv2DTranspose(filters = 3,
                              kernel_size = params.kernel_size,
                              activation = 'sigmoid',
                              padding = 'same',
                              name = 'decoder_output')(x)

    # instantiate decoder model
    decoder_model = Model(latent_inputs, outputs, name = 'decoder')
    decoder_model.summary()

    # instantiate VAE model
    outputs = decoder_model(encoder_model(inputs)[2])
    vae = Model(inputs, outputs, name = 'vae')
    return vae, decoder_model, outputs
