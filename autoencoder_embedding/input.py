from __future__ import print_function
import tensorflow.keras
from tensorflow.keras.datasets import cifar10
from utils import Params

params = Params("./params.json")

class DataGen():

    def __init__(self, params):
        # The data, split between train and test sets:
        (x_train, y_train), (x_test, y_test) = cifar10.load_data()
        print('x_train shape:', x_train.shape)
        print(x_train.shape[0], 'train samples')
        print(x_test.shape[0], 'test samples')

        # Convert class vectors to binary class matrices.
        self.y_train = tensorflow.keras.utils.to_categorical(y_train, params.num_classes)
        self.y_test = tensorflow.keras.utils.to_categorical(y_test, params.num_classes)

        self.x_train = x_train.astype('float32') /  255.
        self.x_test = x_test.astype('float32') /  255.




