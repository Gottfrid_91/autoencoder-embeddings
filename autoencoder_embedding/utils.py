"""General utility functions"""
from __future__ import print_function
import json
import glob as glob
import os
import shutil
import tensorflow
from sklearn.manifold import TSNE
import matplotlib.pyplot as plt
import numpy as np
import pandas as pd
import seaborn as sns
import umap


class Params():
    """Class that loads hyperparameters from a json file.
    Example:
    ```
    params = Params(json_path)
    print(params.learning_rate)
    params.learning_rate = 0.5  # change the value of learning_rate in params
    ```
    """

    def __init__(self, json_path):
        self.update(json_path)

    def save(self, json_path):
        """Saves parameters to json file"""
        with open(json_path, 'w') as f:
            json.dump(self.__dict__, f, indent = 4)

    def update(self, json_path):
        """Loads parameters from json file"""
        with open(json_path) as f:
            params = json.load(f)
            self.__dict__.update(params)

    @property
    def dict(self):
        """Gives dict-like access to Params instance by `params.dict['learning_rate']`"""
        return self.__dict__


class Logging():

    def __init__(self, logging_directory, params):
        self.log_dir = logging_directory
        self.model_directory = None
        self.tensorboard_directory = None
        self.params = params

    def __create_dir(self, dir):
        os.makedirs(dir)

    def __create_tensorboard_dir(self, model_dir):

        # set abs path to new dir
        new_dir = os.path.join(model_dir, "tensorboard_dir")

        # create new dir
        self.__create_dir(new_dir)

        # set object instance to new path
        self.tensorboard_directory = new_dir

    def __remove_empty_directories(self):

        # get current directories
        current_directories = glob.glob(self.log_dir + "/*")

        # check for each dir, if weight.hdf5 file is contained
        for current_directory in current_directories:
            if not os.path.isfile(os.path.join(current_directory, "model.h5")):
                # remove directory
                shutil.rmtree(current_directory)

    def create_model_directory(self):
        '''
        :param logging_directory: string, gen directory for logging
        :return: None
        '''

        # remove emtpy directories
        self.__remove_empty_directories()

        # get allready created directories
        existing_ = os.listdir(self.log_dir)

        # if first model iteration, set to zero
        if existing_ == []:
            new = 0
            # save abs path of created dir
            created_dir = os.path.join(self.log_dir, str(new))

            # make new directory
            self.__create_dir(created_dir)

            # create subdir for tensorboard logs
            self.__create_tensorboard_dir(created_dir)

        else:
            # determine the new model directory
            last_ = max(list(map(int, existing_)))
            new = int(last_) + 1

            # save abs path of created dir
            created_dir = os.path.join(self.log_dir, str(new))

            # make new directory
            self.__create_dir(created_dir)

            # create subdir for tensorboard logs
            self.__create_tensorboard_dir(created_dir)

        # set class instancy to hold abs path
        self.model_directory = created_dir

    def save_dict_to_json(self, json_path):
        """Saves dict of floats in json file
        Args:
            d: (dict) of float-castable values (np.float, int, float, etc.)
            json_path: (string) path to json file
        """
        with open(json_path, 'w') as f:
            # We need to convert the values to float for json (it doesn't accept np.array, np.float, )
            d = {k: str(v) for k, v in self.params.dict.items()}
            json.dump(d, f, indent = 4)


class Evaluation():
    '''
    labels: list, integers
    predictions: list, integers
    history: pandas data frame
    '''

    def __init__(self, params):
        self.model_directory = None
        self.tensorboard_directory = None
        self.params = params
        pass

    def get_embeddings(self, model, input_data):
        ind = len(model.layers)
        get_tensor_values = tensorflow.keras.backend.function([model.layers[0].input],
                                                              [model.layers[int(ind / 2)].output])

        embeddings = []
        for i in range(input_data.shape[0]):
            embedding = get_tensor_values([input_data[i, :, :, :].reshape(1, 32, 32, 3)])[0]
            embedding = embedding.reshape(-1)
            embeddings.append(embedding)
        return np.array(embeddings)

    def visualize_embedding(self, model, datagen, embedding_algorithm):
        # get bottleneck embedding
        embeddings = self.get_embeddings(model, datagen.x_test)

        if embedding_algorithm == "TSNE":
            plot_embeddings = TSNE(n_components = 2).fit_transform(embeddings)

        if embedding_algorithm == "UMAP":
            plot_embeddings = umap.UMAP(n_components = 2).fit_transform(embeddings)

        if embedding_algorithm == "None":
            plot_embeddings = embeddings

        # get labels
        labels = np.argmax(datagen.y_test, axis = 1).tolist()

        # create labeled x and y data
        data = pd.DataFrame({"x": plot_embeddings[:, 0].tolist(), "y": plot_embeddings[:, 1].tolist(), "label": labels})

        # create a new figure
        plt.figure(figsize = (16, 10))
        sns.scatterplot(
            x = "x", y = "y",
            hue = "label",
            palette = sns.color_palette("hls", np.unique(labels).shape[0]),
            data = data,
            legend = "full",
            alpha = 0.3
        )
        plt.savefig(self.params.model_dir + "/output.png")

    def plot_examples(self, to_plot, name=""):
        fig = plt.figure(figsize = (8, 8))
        columns = 2
        rows = 1
        types = ["image", "prediction"]

        for i in range(1, columns * rows + 1):
            img = to_plot[i - 1]
            fig.add_subplot(rows, columns, i)
            plt.imshow(img.reshape(32, 32, 3))
            plt.title(name + ": " + types)

        plt.savefig(self.params.model_dir + "/train_examples_{}.png".format(name))


class TrainOps():
    def __init__(self, params):
        self.params = params

    def step_decay(self, epoch):
        """Learning Rate Schedule

        Learning rate is scheduled to be reduced after 80, 120, 160, 180 epochs.
        Called automatically every epoch as part of callbacks during training.

        # Arguments
            epoch (int): The number of epochs

        # Returns
            lr (float32): learning rate
        """
        lr = self.params.learning_rate
        if epoch >= int(self.params.epochs / 3):
            lr *= 5e-1
        if epoch >= int(self.params.epochs / 2):
            lr *= 2e-1
        if epoch >= int(self.params.epochs / 3) * 2:
            lr *= 5e-1
        if epoch >= int((self.params.epochs / 3) * 2.25):
            lr *= 2e-1
        print('Learning rate: ', lr)
        return lr

    def callbackslist(self, logging_dir):
        '''callbacks'''
        lr_scheduler = tensorflow.keras.callbacks.LearningRateScheduler(self.step_decay)
        csv_logger = tensorflow.keras.callbacks.CSVLogger(filename = logging_dir + '/history.csv',
                                                          append = True,
                                                          separator = ",")

        checkpoint = tensorflow.keras.callbacks.ModelCheckpoint(filepath = logging_dir + "/model.h5",
                                                                monitor = 'val_loss',
                                                                save_best_only = True,
                                                                verbose = 1,
                                                                save_weights_only = False)

        tb = tensorflow.keras.callbacks.TensorBoard(log_dir = logging_dir + "/tensorboard",
                                                    histogram_freq = 0,
                                                    write_graph = True,
                                                    write_images = True,
                                                    embeddings_freq = 0,
                                                    embeddings_layer_names = None,
                                                    embeddings_metadata = None)

        return [lr_scheduler, csv_logger, checkpoint, tb]
