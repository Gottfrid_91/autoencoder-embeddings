from tensorflow.keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D, Flatten, Reshape
from tensorflow.keras.models import Model
from tensorflow.keras import regularizers


def autoencoder(params):
    input_img = Input(shape = (32, 32, 3))  # adapt this if using `channels_first` image data format

    x = Conv2D(params.num_filters, (3, 3), activation = 'relu', padding = 'same')(input_img)
    x = MaxPooling2D((2, 2), padding = 'same')(x)
    x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
    x = MaxPooling2D((2, 2), padding = 'same')(x)
    x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)

    if params.deep:
        for i in range(0, 3):
            x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
            x = MaxPooling2D((2, 2), padding = 'same')(x)
    else:
        x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
        x = MaxPooling2D((2, 2), padding = 'same')(x)

    encoded_flatten = Flatten()(x)

    if (params.sparse == True) and (params.deep == False):
        encoded_flatten = Dense(256, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        encoded_flatten = Dense(256, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        x = Reshape((4, 4, 16))(encoded_flatten)

    if (params.sparse == True) and (params.deep == True):
        encoded_flatten = Dense(64, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        encoded_flatten = Dense(64, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        x = Reshape((1, 1, 64))(encoded_flatten)

    if (params.sparse == False) and (params.deep == True):
        encoded_flatten = Dense(64)(encoded_flatten)
        encoded_flatten = Dense(64)(encoded_flatten)
        x = Reshape((1, 1, 64))(encoded_flatten)

    if (params.sparse == False) and (params.deep == False):
        encoded_flatten = Dense(256)(encoded_flatten)
        encoded_flatten = Dense(256)(encoded_flatten)
        x = Reshape((4, 4, 16))(encoded_flatten)

    # Decoder
    # at this point the representation is (4, 4, 8) i.e. 128-dimensional
    if params.deep:
        for i in range(0, 3):
            x = Conv2D(params.num_filters * 4, (3, 3), activation = 'relu', padding = 'same')(x)
            x = UpSampling2D((2, 2), data_format = 'channels_last')(x)
    else:
        x = Conv2D(params.num_filters * 4, (3, 3), activation = 'relu', padding = 'same')(x)
        x = UpSampling2D((2, 2),
                         data_format = 'channels_last')(x)

    x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
    x = UpSampling2D((2, 2),
                     data_format = 'channels_last')(x)
    x = Conv2D(params.num_filters, (3, 3), activation = 'relu', padding = 'same')(x)
    x = UpSampling2D((2, 2),
                     data_format = 'channels_last')(x)
    decoded = Conv2D(3, (3, 3), activation = 'sigmoid', padding = 'same')(x)

    autoencoder = Model(input_img, decoded)
    return autoencoder


def latent_autoencoder(params):
    input_img = Input(shape = (32, 32, 3))  # adapt this if using `channels_first` image data format

    x = Conv2D(params.num_filters, (3, 3), activation = 'relu', padding = 'same')(input_img)
    x = MaxPooling2D((2, 2), padding = 'same')(x)
    x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
    x = MaxPooling2D((2, 2), padding = 'same')(x)
    x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)

    if params.deep:
        for i in range(0, 3):
            x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
            x = MaxPooling2D((2, 2), padding = 'same')(x)
    else:
        x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
        x = MaxPooling2D((2, 2), padding = 'same')(x)

    encoded_flatten = Flatten()(x)

    if (params.sparse == True) and (params.deep == False):
        encoded_flatten = Dense(16, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        encoded_flatten = Dense(2, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        encoded_flatten = Dense(16, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        x = Reshape((4, 4, 16))(encoded_flatten)

    if (params.sparse == True) and (params.deep == True):
        encoded_flatten = Dense(16, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        encoded_flatten = Dense(2, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        encoded_flatten = Dense(16, activity_regularizer = regularizers.l1(params.reg))(encoded_flatten)
        x = Reshape((1, 1, 16))(encoded_flatten)

    if (params.sparse == False) and (params.deep == True):
        encoded_flatten = Dense(16)(encoded_flatten)
        encoded_flatten = Dense(2)(encoded_flatten)
        encoded_flatten = Dense(16)(encoded_flatten)
        x = Reshape((1, 1, 16))(encoded_flatten)

    if (params.sparse == False) and (params.deep == False):
        encoded_flatten = Dense(16)(encoded_flatten)
        encoded_flatten = Dense(2)(encoded_flatten)
        encoded_flatten = Dense(16)(encoded_flatten)
        x = Reshape((4, 4, 16))(encoded_flatten)

    # Decoder
    # at this point the representation is (4, 4, 8) i.e. 128-dimensional
    if params.deep:
        for i in range(0, 2):
            x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
            x = UpSampling2D((2, 2), data_format = 'channels_last')(x)
    else:
        x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
        x = UpSampling2D((2, 2),
                         data_format = 'channels_last')(x)

    x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
    x = UpSampling2D((2, 2),
                     data_format = 'channels_last')(x)
    x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
    x = UpSampling2D((2, 2),
                     data_format = 'channels_last')(x)
    x = Conv2D(params.num_filters, (3, 3), activation = 'relu', padding = 'same')(x)
    x = UpSampling2D((2, 2),
                     data_format = 'channels_last')(x)
    decoded = Conv2D(3, (3, 3), activation = 'sigmoid', padding = 'same')(x)

    autoencoder = Model(input_img, decoded)
    return autoencoder
