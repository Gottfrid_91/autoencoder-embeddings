from __future__ import print_function
from utils import Params, Evaluation
from input import DataGen
import tensorflow
import random

params = Params("params.json")

# set params string paths before init other utils classes
model_dir = "/home/olle/PycharmProjects/auto-encoders/autoencoder_embedding/logs/10"
params.model_dir = model_dir

datagen = DataGen(params = params)
evaluation = Evaluation(params)

# load selected model
model = tensorflow.keras.models.load_model(params.model_dir + "/model.h5")

# plot and get embeddings
evaluation.visualize_embedding(model, datagen, embedding_algorithm = "TSNE")

# predict random train and test images
select = random.randint(0, datagen.x_train.shape[0])

image = datagen.x_train[select, :, :, :]
prediction = model.predict(image.reshape(1, 32, 32, 3))

to_plot = [image, prediction]

evaluation.plot_examples(to_plot, "train_1")

# predict random train and test images
select = random.randint(0, datagen.x_train.shape[0])

image = datagen.x_train[select, :, :, :]
prediction = model.predict(image.reshape(1, 32, 32, 3))

to_plot = [image, prediction]

evaluation.plot_examples(to_plot, "train_2")

# predict random train and test images
select = random.randint(0, datagen.x_test.shape[0])

image = datagen.x_test[select, :, :, :]
prediction = model.predict(image.reshape(1, 32, 32, 3))

to_plot = [image, prediction]

evaluation.plot_examples(to_plot, "test_1")

# predict random train and test images
select = random.randint(0, datagen.x_test.shape[0])

image = datagen.x_test[select, :, :, :]
prediction = model.predict(image.reshape(1, 32, 32, 3))

to_plot = [image, prediction]

evaluation.plot_examples(to_plot, "test_2")
