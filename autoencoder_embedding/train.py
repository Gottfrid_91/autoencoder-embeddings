from __future__ import print_function
from autoencoder import autoencoder, latent_autoencoder
import os
from utils import TrainOps, Params, Logging, Evaluation
from input import DataGen
import tensorflow
import random

params = Params("./params.json")
trainops = TrainOps(params)
logging = Logging("./logs", params)
datagen = DataGen(params)

filters = [32, 64, 128]
deep = [True]
sparse = [True, False]
regs = [10e-5, 10e-4, 10e-3]

for reg in regs:
    for d in deep:
        for s in sparse:
            for filter_ in filters:

                logging.create_model_directory()

                # set params
                params.deep = d
                params.sparse = s
                params.num_filters = filter_
                params.reg = reg
                params.model_dir = logging.model_directory

                # saving model config file to model output dir
                logging.save_dict_to_json(logging.model_directory + "/config.json")

                model_name = 'model.h5'

                # define callbacks
                cb = trainops.callbackslist(logging.model_directory)

                # load model
                model = latent_autoencoder(params)

                model.summary()
                model.compile(optimizer = 'adam', loss = 'mse')

                # Fit the model
                model.fit(datagen.x_train,
                            datagen.x_train,
                            batch_size = params.batch_size,
                            validation_data = (datagen.x_test, datagen.x_test),
                            epochs = params.epochs,
                            callbacks = cb)

                model_path = os.path.join(logging.model_directory, model_name)

                # Score trained model.
                scores = model.evaluate(datagen.x_test, datagen.x_test, verbose = 1)
                print('Test loss:', scores)

                print("-------------------------------------- EVALUATE ALGORITHM -----------------------------------------")
                # get all evaluate functions
                evaluation = Evaluation(params)

                # plot and get embeddings
                evaluation.visualize_embedding(model, datagen, embedding_algorithm = "None")

                # predict random train and test images
                select = random.randint(0, datagen.x_train.shape[0])

                image = datagen.x_train[select, :, :, :]
                prediction = model.predict(image.reshape(1, 32, 32, 3))

                to_plot = [image, prediction]

                evaluation.plot_examples(to_plot, "train_1")

                # predict random train and test images
                select = random.randint(0, datagen.x_train.shape[0])

                image = datagen.x_train[select, :, :, :]
                prediction = model.predict(image.reshape(1, 32, 32, 3))

                to_plot = [image, prediction]

                evaluation.plot_examples(to_plot, "train_2")

                # predict random train and test images
                select = random.randint(0, datagen.x_test.shape[0])

                image = datagen.x_test[select, :, :, :]
                prediction = model.predict(image.reshape(1, 32, 32, 3))

                to_plot = [image, prediction]

                evaluation.plot_examples(to_plot, "test_1")

                # predict random train and test images
                select = random.randint(0, datagen.x_test.shape[0])

                image = datagen.x_test[select, :, :, :]
                prediction = model.predict(image.reshape(1, 32, 32, 3))

                to_plot = [image, prediction]

                evaluation.plot_examples(to_plot, "test_2")

                tensorflow.keras.backend.clear_session()
