from tensorflow.keras.layers import Input, Dense, Conv2D, MaxPooling2D, UpSampling2D, Flatten, Reshape
from tensorflow.keras.models import Model


def model(params):
    input_img = Input(shape = (32, 32, 3))  # adapt this if using `channels_first` image data format

    x = Conv2D(params.num_filters, (3, 3), activation = 'relu', padding = 'same')(input_img)
    x = MaxPooling2D((2, 2), padding = 'same')(x)
    x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
    x = MaxPooling2D((2, 2), padding = 'same')(x)
    x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)

    for i in range(0, 3):
        x = Conv2D(params.num_filters * 2, (3, 3), activation = 'relu', padding = 'same')(x)
        x = MaxPooling2D((2, 2), padding = 'same')(x)

    flatten_x = Flatten()(x)
    output = Dense(params.num_classes, activation = "softmax")(flatten_x)
    return Model(input_img, [output])