from __future__ import print_function
from model import model
import os
from utils import TrainOps, Params, Logging, Evaluation
from input import DataGen
import tensorflow
import random

params = Params("./params.json")
trainops = TrainOps(params)
logging = Logging("./logs", params)
datagen = DataGen(params)

filters = [32, 64, 128]
deep = [True, False]
sparse = [True, False]


logging.create_model_directory()

# set params
params.num_filters = filters[2]
params.model_dir = logging.model_directory

# saving model config file to model output dir
logging.save_dict_to_json(logging.model_directory + "/config.json")

model_name = 'model.h5'

# define callbacks
cb = trainops.callbackslist(logging.model_directory)

# load model
model = model(params)

model.summary()
model.compile(optimizer = 'adam', loss = 'categorical_crossentropy', metrics = ['accuracy'])


# Fit the model
model.fit(datagen.x_train,
            datagen.y_train,
            batch_size = params.batch_size,
            validation_data = (datagen.x_test, datagen.y_test),
            epochs = params.epochs,
            callbacks = cb)


model_path = os.path.join(logging.model_directory, model_name)

# Score trained model.
scores = model.evaluate(datagen.x_test, datagen.y_test, verbose = 1)
print('Test loss:', scores[0])
print('Test accuracy:', scores[1])

print("-------------------------------------- EVALUATE ALGORITHM -----------------------------------------")
# get all evaluate functions
evaluation = Evaluation(params)

# plot and get embeddings
evaluation.visualize_embedding(model, datagen, embedding_algorithm = "TSNE")

tensorflow.keras.backend.clear_session()

