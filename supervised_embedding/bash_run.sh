#!/bin/bash

#SBATCH -o ./out_ae.txt
#SBATCH -e ./error_ae.txt
#SBATCH -J e-d
#SBATCH --mem=2G
#SBATCH -p icb_gpu
#SBATCH --gres=gpu:1
#SBATCH -t 40:00:00
#SBATCH --nice=100

conda activate tf_2
python3.6 train.py

